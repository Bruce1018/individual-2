# Individual project 2

> Bruce Xu  
> zx112

# Demo video
https://www.youtube.com/watch?v=CMZfNpgF_WY

# Requirements and steps

## Simple REST API/web service in Rust

Create a new cargo directory.

Edit the `.toml` and `.rs` file to implement `Fib Fuction`

Here are the pictures

![](/img/local.png)

**Command Used**

```bash
cargo new xxx

cargo test

cargo run
```

## Dockerfile to containerize service

Create `Dockerfile` and edit it 

```makefile
# Build stage
FROM rust:1.68 AS build
WORKDIR /app
COPY . .
RUN cargo build --release

FROM gcr.io/distroless/cc-debian11
COPY --from=build /app/target/release/week4-mini-proj /app/

# use non-root user
USER nonroot:nonroot

# Set up App directory
ENV APP_HOME=/app
WORKDIR $APP_HOME

# Expose the port
EXPOSE 8080

# Run the web service on container startup.
ENTRYPOINT [ "/app/week4-mini-proj" ]

```

Run the following command 

```bash
docker build -t my-rust-app .

docker ps

docker run -p 8080:8080 my-rust-app
```

You should see following

![](/img/docker.png)

![](/img/containers.png)

## CI/CD pipeline files

create `.yml` file and edit it 

![](/img/ci.png)
